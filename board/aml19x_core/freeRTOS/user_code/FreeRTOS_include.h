/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief FreeRTOS include
 *
 * \internal
 * \par Modification history
 * - 1.00 20-05-14  yrz, first implementation
 * \endinternal
 */
#ifndef __FREERTOS_INCLUDE_H
#define __FREERTOS_INCLUDE_H

#include "stdarg.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"

#endif /* __FREERTOS_INCLUDE_H */

/* end of file */
