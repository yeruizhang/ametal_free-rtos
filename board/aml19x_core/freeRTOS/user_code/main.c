/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief HC32L19x 模板工程
 *
 * - 实验现象:
 *   模板例程:LED0以1秒的频率闪烁; demo例程入口:详见demo入口函数文件介绍
 *
 * \internal
 * \par Modification history
 * - 1.00 19-10-17  zp, first implementation
 * \endinternal
 */

/**
 * \brief 例程入口
 */
#include "ametal.h"
#include "am_board.h"
#include "am_vdebug.h"
#include "am_delay.h"
#include "am_gpio.h"
#include "hc32_pin.h"
#include "hc32_periph_map.h"
#include "freertos_include.h"
#include "am_hc32l19x_inst_init.h"

static TaskHandle_t HandleLed = NULL;
static TaskHandle_t HandleTask = NULL;

static void task_led (void *p_arg);
static void task_printf (void *p_arg);

/*******************************************************************************
 * 主函数
 ******************************************************************************/
int am_main (void)
{
    AM_DBG_INFO("Start up successful!\r\n");

    xTaskCreate(task_printf,
                "task_printf",
                512,
                NULL,
                4,
                &HandleTask);

    xTaskCreate(task_led,
                "task_led",
                128,
                NULL,
                3,
                &HandleLed);

    vTaskStartScheduler();
    
    while(1);
}

static void task_printf (void *p_arg)
{
    while (1) {
        AM_DBG_INFO("task running !\r\n");
        vTaskDelay(500);
    }
}

static void task_led (void *p_arg)
{
    while (1) {
        AM_DBG_INFO("led running !");
        am_led_toggle(LED0);
        vTaskDelay(500);
    }
}

/* end of file */
